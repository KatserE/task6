// MARK: Class that describes reference to value type
class Ref<T> {
    var value: T
    init(value: T) {
        self.value = value
    }
}

// MARK: Class that describes collection
struct IOSCollection<T> : Sequence {
    // MARK: Class that describes the buffer of collection
    struct Buffer<T> {
        var buffer: [T]
        var count: Int { buffer.count }

        subscript(index: Int) -> T {
            get {
                assert(indexIsValid(index: index), "Index out of bounds!")
                return buffer[index]
            }
            set {
                assert(indexIsValid(index: index), "Index out of bounds!")
                buffer[index] = newValue
            }
        }

        private func indexIsValid(index: Int) -> Bool {
            index >= 0 && index < count
        }
    }

    // MARK: Class that implements iterator for IOSCollection object
    struct Iterator<T> : IteratorProtocol {
        private let _ref: Ref<Buffer<T>>
        private let _end: Int
        private var _curIndex = 0

        init(ref: Ref<Buffer<T>>) {
            _ref = ref
            _end = ref.value.count
        }

        mutating func next() -> T? {
            guard _curIndex < _end else { return nil }

            defer { _curIndex += 1 }
            return _ref.value[_curIndex]
        }
    }

    private var _ref: Ref<Buffer<T>>

    init(repeating value: T, count: Int) {
        _ref = Ref(value: Buffer(buffer: Array(repeating: value, count: count)))
    }

    var count: Int { _ref.value.count }
    subscript(index: Int) -> T {
        get {
            _ref.value[index]
        }
        set {
            if !isKnownUniquelyReferenced(&_ref) {
                _ref = Ref<Buffer<T>>(value: _ref.value)
            }

            _ref.value[index] = newValue
        }
    }

    func makeIterator() -> Iterator<T> {
        return Iterator(ref: _ref)
    }

    func bufferAddress() -> String {
        "\(Unmanaged.passUnretained(_ref).toOpaque())"
    }
}

var collection1 = IOSCollection(repeating: 5, count: 10)
var collection2 = collection1

print("Before change:")
print("Addresses of collections buffers:")
print("Collection 1 -- \(collection1.bufferAddress())")
print("Collection 2 -- \(collection2.bufferAddress())")

print("Values:")
print("Collection 1: ", terminator: "")
collection1.forEach { print($0, terminator: " ") }
print("\nCollection 2: ", terminator: "")
collection2.forEach { print($0, terminator: " ") }

print("\n\nAfter change:")
collection2[3] = 7

print("Addresses of collections buffers:")
print("Collection 1 -- \(collection1.bufferAddress())")
print("Collection 2 -- \(collection2.bufferAddress())")

print("Values:")
print("Collection 1: ", terminator: "")
collection1.forEach { print($0, terminator: " ") }
print("\nCollection 2: ", terminator: "")
collection2.forEach { print($0, terminator: " ") }
print()
