# Основная информация
Для этого задания также было решено не создавать отдельный проект с помощью
xcode, чтобы не загромождать список проектов и не плодить кеши.

# Задание
1. Почитать про copy on write (CoW) и понять, что это такое;
2. Реализовать структуру `IOSCollection` и создать в ней CoW по типу -
[Видео-урок](https://www.youtube.com/embed/QsoqHGgX2rE?start=594);
3. Создать протокол `Hotel` с инициализатором, который принимает `roomCount`, после
создать `class HotelAlfa` добавить свойство `roomCount` и подписаться на этот протокол;
4. Создать `protocol GameDice` у него `{ get }` свойство `numberDice` далее нужно
расширить `Int` так, чтобы когда мы напишем такую конструкцию `let diceCoub =
4 diceCoub.numberDice` в консоле мы увидели такую строку - `'Выпало 4 на кубике'`;
5. Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional,
создать класс, подписать на протокол и реализовать только 1 обязательное свойство;
6. Изучить раздел 'Протоколы -> Делегирование' в документации;
7. Проработать код из видео;
8. Создать 2 протокола: со свойствами 'время', 'количество кода' и функцией
`writeCode(platform: Platform, numberOfSpecialist: Int);` и другой с функцией:
`stopCoding()`. Создайте класс: 'Компания', у которого есть свойства - 'количество
программистов', 'специализация' (ios, android, web);
9. Компании подключаем два этих протокола;
10.  Задача: вывести в консоль сообщения - `'разработка началась. пишем код <такой-то>'`
и `'работа закончена. Сдаю в тестирование'`, попробуйте обработать крайние случаи.

# Сборка
Для сборки введите:
```bash
cd task6
make
```

# Запуск
Для запуска программы, в которой реализован механизм CoW для своей коллекции введите:
```bash
./output/task6_cow
```

Для запуска программы, в которой реализованы решения для заданий с протоколами введите:
```bash
./output/task6_rest
```

# Очистка
Для очистки введите:
```bash
make clean
```
