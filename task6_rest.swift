// MARK: - Create Hotel protocol with initializer. Create HotelAlfa that
// subscribe on Hotel
// MARK: Hotel protocol with initializer
protocol Hotel {
    init(roomCount: Int)
}

// MARK: HotelAlfa class that subscribes on Hotel protocol
class HotelAlfa : Hotel {
    let roomCount: Int

    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}


// MARK: - Create GameDice protocol with { get } property
protocol GameDice {
    var numberDice: Void { get }
}

// MARK: Create Int extension to print value when property is called
extension Int : GameDice {
    var numberDice: Void {
        print("Dropped \(self) on the dice")
    }
}

print("Roll a dice:")
let diceCoub = 4
diceCoub.numberDice


// MARK: - Create protocol with an option property
import Foundation
@objc protocol ProtocolWithOptional {
    var nonOptionalProperty: Int { get }
    @objc optional var optionalProperty: Int { get }

    func method()
}

// MARK: Create a class that adopts protocol
class Adopter : ProtocolWithOptional {
    let nonOptionalProperty: Int = 5

    func method() {
        print("This is the class without implementing a protocol optional requirement")
    }
}


// MARK: - Create two protocols. After it cleare class Company
// MARK: Enumeration that describes possible platforms
enum Platform {
    case IOS, Android, Web
}

protocol TaskConstraints {
    var days: UInt { get }
    var amountOfCode: UInt { get }
}

struct CodingTaskConstraints : TaskConstraints {
    var days: UInt
    var amountOfCode: UInt
}

protocol CodingProcessDelegate {
    mutating func startCoding(platform: Platform, numberOfSpecialist: Int)
    mutating func inWork()
    func stopCoding()
}

struct CodingProcessLogger : CodingProcessDelegate {
    private var _platform: Platform = .IOS
    private var _numOfDays = 0

    mutating func startCoding(platform: Platform, numberOfSpecialist: Int) {
        _platform = platform
        _numOfDays = 0

        print("Project to platform \(platform) is started developing by " +
              "\(numberOfSpecialist) programmers.")
    }

    mutating func inWork() {
        _numOfDays += 1
    }

    func stopCoding() {
        print("Project to \(_platform) platform is done! The work has done for \(_numOfDays) days.")
    }
}

enum TaskHandleError : Error {
    case NotEnoughTime, ThereIsNoTask
}

class Company {
    let specialization: Platform
    let teamSize: UInt
    let codePerDayBySpecialist: UInt = 200

    var task: TaskConstraints?
    var delegate: CodingProcessDelegate?

    init(specialization: Platform, teamSize: UInt) {
        self.specialization = specialization
        self.teamSize = teamSize
    }

    func startJob() throws {
        guard let curTask = task else {
            throw TaskHandleError.ThereIsNoTask
        }
        guard checkIsPossible(task: curTask) else {
            throw TaskHandleError.NotEnoughTime
        }

        delegate?.startCoding(platform: specialization, numberOfSpecialist: Int(teamSize))
        let needDays = curTask.amountOfCode / (teamSize * codePerDayBySpecialist)

        for _ in 0..<(max(1, needDays)) {
            delegate?.inWork()
        }

        delegate?.stopCoding()
        task = nil
    }

    private func checkIsPossible(task: TaskConstraints) -> Bool {
        return task.days * codePerDayBySpecialist * teamSize >= task.amountOfCode
    }
}

print("\nIOS company is performing tasks:")
let company = Company(specialization: .IOS, teamSize: 10)
company.delegate = CodingProcessLogger()

do {
    company.task = CodingTaskConstraints(days: 1, amountOfCode: 20_000)
    try company.startJob()
} catch TaskHandleError.NotEnoughTime {
}

company.task = CodingTaskConstraints(days: 1, amountOfCode: 1_000)
try company.startJob()

print() // To separate the output
company.task = CodingTaskConstraints(days: 1, amountOfCode: 2_000)
try company.startJob()

print() // To separate the output
company.task = CodingTaskConstraints(days: 5, amountOfCode: 10_000)
try company.startJob()

do {
    try company.startJob()
} catch TaskHandleError.ThereIsNoTask {
}

do {
    company.task = CodingTaskConstraints(days: 1, amountOfCode: 2_001)
    try company.startJob()
} catch TaskHandleError.NotEnoughTime {
}

do {
    company.task = CodingTaskConstraints(days: 0, amountOfCode: 1)
    try company.startJob()
} catch TaskHandleError.NotEnoughTime {
}

do {
    company.task = CodingTaskConstraints(days: 5, amountOfCode: 10_001)
    try company.startJob()
} catch TaskHandleError.NotEnoughTime {
}
