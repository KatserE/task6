OUTPUT		:= "./output/"

COW_SRC		:= "task6_cow.swift"
COW_NAME	:= "task6_cow"

PROTOCOL_TESTS_SCR	:= "task6_rest.swift"
PROTOCOL_TESTS_NAME	:= "task6_rest"

all: output_dir $(COW_NAME) $(PROTOCOL_TESTS_NAME)
	@echo Compilation is successful!

output_dir:
	@test -d $(OUTPUT) || mkdir $(OUTPUT)

$(COW_NAME):
	@swiftc $(COW_SRC) -o $(OUTPUT)/$(COW_NAME)

$(PROTOCOL_TESTS_NAME):
	@swiftc $(PROTOCOL_TESTS_SCR) -o $(OUTPUT)/$(PROTOCOL_TESTS_NAME)

clean:
	@test -d $(OUTPUT) && rm -rf $(OUTPUT)
